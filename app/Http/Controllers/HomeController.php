<?php

namespace app\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function displayGames($date = null)
    {
        $schedule = json_decode(file_get_contents(storage_path().'/SeasonSchedule-20152016.json'), true);

        $game = array();

        if ($date == null) {
            $date = date("d-m-Y");
        }

        $x = 0;
        foreach ($schedule as $nhlGame) {
            $gameTimeString = strtotime($nhlGame['est']);
            $gameDate = date("d-m-Y", $gameTimeString);
            $gameTime = date("H:i", $gameTimeString);
            if ($date == $gameDate) {
                $wat = explode(':', date("H:i", strtotime($nhlGame['est'])));
                $game[$x]['GMT'] = date('H', mktime($wat[0]+5)).':'.$wat[1];
                $game[$x]['EST'] = $gameTime;
                // replace ID's
                if (preg_match('/201502/', $nhlGame['id'])) {
                    $part = 'RS';
                    $seasonPart = '201502';
                } else {
                    $part = 'PS';
                    $seasonPart = '201503';
                }
                $nhlGame['id'] = str_replace('201502', '', $nhlGame['id']);
                $nhlGame['id'] = str_replace('201503', '', $nhlGame['id']);
                $game[$x]['ID'] = $nhlGame['id'];
                $game[$x]['part'] = $part;
                $game[$x]['away'] = $this->replaceTeamNames($nhlGame['a']);
                $game[$x]['home'] = $this->replaceTeamNames($nhlGame['h']);
                if ($game[$x]['part'] == 'RS') {
                    if (@file_get_contents("http://smb.cdnak.neulion.com/fs/nhl/mobile/feed_new/data/streams/2015/ipad/02_".$game[$x]['ID'].".json")) {
                        $gameInfo = json_decode(file_get_contents("http://smb.cdnak.neulion.com/fs/nhl/mobile/feed_new/data/streams/2015/ipad/02_".$game[$x]['ID'].".json"), true);
                    } else {
                        $gameInfo['finish'] = 'false';
                        $gameInfo['gameState'] = '0';
                        $gameInfo['period'] = '0';
                    }
                } else {
                    if (@file_get_contents("http://smb.cdnak.neulion.com/fs/nhl/mobile/feed_new/data/streams/2015/ipad/03_".$game[$x]['ID'].".json")) {
                        $gameInfo = json_decode(file_get_contents("http://smb.cdnak.neulion.com/fs/nhl/mobile/feed_new/data/streams/2015/ipad/03_".$game[$x]['ID'].".json"), true);
                    } else {
                        $gameInfo['finish'] = 'false';
                        $gameInfo['gameState'] = '0';
                        $gameInfo['period'] = '0';
                    }
                }
                $game[$x]['gameinfo'] = $gameInfo;
//                $scoreInfo = $this->getScores($seasonPart, $game[$x]['ID']);
//                $game[$x]['awayScore'] = $scoreInfo['a']['tot']['g'];
//                $game[$x]['homeScore'] = $scoreInfo['h']['tot']['g'];
            }
            $x++;
        }

//        echo'<pre>';
//        print_r($game);
//        echo'</pre>';
        //return View::make('NHL.index', array('date' => $date, 'game' => $game));

        return view('NHL.index', [
            'date'  => $date,
            'game'  => $game,
        ]);
    }

    function getDownloadLink($team) {
        $date = date("d-m-Y", strtotime(date("d-m-Y").'-1 day')); // yesterday!
        $seasonSchedule = json_decode(file_get_contents(storage_path().'/SeasonSchedule-20152016.json'), true);
        foreach ($seasonSchedule as $nhlGame) {
            $gameTimeString = strtotime($nhlGame['est']);
            $gameDate = date("d-m-Y", $gameTimeString);
            if($date == $gameDate && ($nhlGame['a'] == $team || $nhlGame['h'] == $team)) {
                $nhlOutput = array();
                if ($nhlGame['a'] == $team) {
                    $nhlOutput['ha'] = 'away';
                } elseif ($nhlGame['h'] == $team) {
                    $nhlOutput['ha'] = 'home';
                }
                if (preg_match('/201502/', $nhlGame['id'])) { $part = 'RS';} else { $part = 'PS';}

                $nhlGame['id'] = str_replace('201502', '', $nhlGame['id']);
                $nhlGame['id'] = str_replace('201503', '', $nhlGame['id']);

                if ($part == 'RS') {
                    if (@file_get_contents("http://smb.cdnak.neulion.com/fs/nhl/mobile/feed_new/data/streams/2015/ipad/02_".$nhlGame['id'].".json")) {
                        $gameInfo = json_decode(file_get_contents("http://smb.cdnak.neulion.com/fs/nhl/mobile/feed_new/data/streams/2015/ipad/02_".$nhlGame['id'].".json"), true);
                    } else {

                    }
                } else {
                    if (@file_get_contents("http://smb.cdnak.neulion.com/fs/nhl/mobile/feed_new/data/streams/2015/ipad/03_".$nhlGame['id'].".json")) {
                        $gameInfo = json_decode(file_get_contents("http://smb.cdnak.neulion.com/fs/nhl/mobile/feed_new/data/streams/2015/ipad/03_".$nhlGame['id'].".json"), true);
                    } else {

                    }
                }
                if (!isset($gameInfo['gameStreams']['ipad'][$nhlOutput['ha']]['vod-whole']['bitrate0'])) {
                    if($nhlOutput['ha'] = 'home') {
                        $gameurl = $gameInfo['gameStreams']['ipad']['home']['vod-whole']['bitrate0'];
                    } else {
                        $gameurl = $gameInfo['gameStreams']['ipad']['away']['vod-whole']['bitrate0'];
                    }
                } else {
                    $gameurl = $gameInfo['gameStreams']['ipad'][$nhlOutput['ha']]['vod-whole']['bitrate0'];
                }
                $nhlOutput['url'] = str_replace('.cdnak.','.cdnl3nl.', str_replace('.m3u8','', str_replace('ipad', '5000', $gameurl)));
                $nhlOutput['filename'] = date("Ymd", strtotime(date("d-m-Y").'-1 day')).'_'.$nhlGame['a'].'_'.$nhlGame['h'].'.mp4';
                $nhlOutput['error'] = '0';
            } else {
                if(!isset($nhlOutput['error'])){
                    $nhlOutput['error'] = 'NOGAME';
                }
            }

        }
        return json_encode($nhlOutput);
    }


    public function replaceTeamNames($input)
    {
        $search = array(
        'PIT',
        'TOR',
        'MTL',
        'WSH',
        'CHI',
        'WPG',
        'EDM',
        'PHI',
        'BUF',
        'DET',
        'ANA',
        'COL',
        'TBL',
        'BOS',
        'NJD',
        'CGY',
        'NSH',
        'STL',
        'LAK',
        'MIN',
        'FLA',
        'DAL',
        'NYR',
        'NYI',
        'ARI',
        'VAN',
        'SJS',
        'OTT',
        'CBJ',
        'CAR'
        );

        $replaces = array(
        'Pittsburgh Penguins',
        'Toronto Maple Leafs',
        'Montreal Canadiens',
        'Washington Capitals',
        'Chicago Blackhawks',
        'Winnipeg Jets',
        'Edmonton Oilers',
        'Philadelphia Flyers',
        'Buffalo Sabres',
        'Detroit Red Wings',
        'Anaheim Ducks',
        'Colorado Avalanche',
        'Tampa Bay Lightning',
        'Boston Bruins',
        'New Jersey Devils',
        'Calgary Flames',
        'Nashville Predators',
        'St Louis Blues',
        'LA Kings',
        'Minnesota Wild',
        'Florida Panthers',
        'Dallas Stars',
        'New York Rangers',
        'New York Islanders',
        'Arizona Coyotes',
        'Vancouver Canucks',
        'San Jose Sharks',
        'Ottawa Senators',
        'Colombus Blue Jackets',
        'Carolina Hurricanes'
        );

        $team = str_replace($search, $replaces, $input);

        return $team;
    }
}
