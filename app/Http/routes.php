<?php

//Route::get('/', ['as' => 'index', 'uses' => '']);

Route::get('/', ['as' => 'index', 'uses' => 'HomeController@displayGames']);
Route::get('/{date}', array('as' => 'NHL', 'uses' => 'HomeController@displayGames'));

Route::get('NHLDL/{team}/', array('as' => 'NHLDL', 'uses' => 'HomeController@getDownloadLink'));