@extends('layouts.base')

@section('title')
    | NHL Games
@stop

@section('content')
    <h1>NHL Games</h1>

    <ul class="pager">
        <li class="previous"><a href="/{{ $date && $date !== date("d-m-Y") ? date("d-m-Y", strtotime($date.'-1 day'))
         : date("d-m-Y", strtotime(date("d-m-Y").'-1 day')) }}">← Previous Day</a></li>
        <li class="next"><a href="/{{ $date && $date !== date("d-m-Y") ? date("d-m-Y", strtotime($date.'+1 day')) :
        date("d-m-Y", strtotime(date("d-m-Y").'+1 day')) }}">Next Day →</a></li>
    </ul>

    <h3>{{ $date && $date !== date("d-m-Y") ? "The games on $date are..." : "Tonights games are..." }}</h3>
    <div class="alert alert-info">Links to streams will appear once they are available</div>

    @foreach ($game as $game)
        <div class="game">
            <h3>{{ $game['away'] }} @ {{ $game['home'] }} <br><small>{{ $game['EST'] }} EST, {{ $game['GMT'] }} GMT</small></h3>
            @if ($game['gameinfo']['finish'] == 'true')
                <p><button type="button" class="btn btn-danger">Game has ended</button></p>
                @if (isset($game['gameinfo']['gameStreams']['ipad']['home']['vod-whole']['bitrate0']))
                    <p>
                    <h4>Home Streams</h4>
                    <a href="{{ str_replace('ipad', '4500', $game['gameinfo']['gameStreams']['ipad']['home']['vod-whole']['bitrate0']) }}" class="btn btn-primary">4500</a>
                    <a href="{{ str_replace('ipad', '3000', $game['gameinfo']['gameStreams']['ipad']['home']['vod-whole']['bitrate0']) }}" class="btn btn-primary">3000</a>
                    <a href="{{ str_replace('ipad', '1600', $game['gameinfo']['gameStreams']['ipad']['home']['vod-whole']['bitrate0']) }}" class="btn btn-primary">1600</a>
                    <a href="{{ str_replace('ipad', '800', $game['gameinfo']['gameStreams']['ipad']['home']['vod-whole']['bitrate0']) }}" class="btn btn-primary">800</a>
                    <h4>Home Downloads</h4>
                    <a href="{{ str_replace('.cdnak.','.cdnl3nl.', str_replace('.m3u8','', str_replace('ipad', '5000', $game['gameinfo']['gameStreams']['ipad']['home']['vod-whole']['bitrate0']))) }}" class="btn btn-warning">5000</a>
                    <a href="{{ str_replace('.cdnak.','.cdnl3nl.', str_replace('.m3u8','', str_replace('ipad', '3000', $game['gameinfo']['gameStreams']['ipad']['home']['vod-whole']['bitrate0']))) }}" class="btn btn-warning">3000</a>
                    <a href="{{ str_replace('.cdnak.','.cdnl3nl.', str_replace('.m3u8','', str_replace('ipad', '1600', $game['gameinfo']['gameStreams']['ipad']['home']['vod-whole']['bitrate0']))) }}" class="btn btn-warning">1600</a>
                    <a href="{{ str_replace('.cdnak.','.cdnl3nl.', str_replace('.m3u8','', str_replace('ipad', '800', $game['gameinfo']['gameStreams']['ipad']['home']['vod-whole']['bitrate0']))) }}" class="btn btn-warning">800</a>
                    </p>
                @else
                    <p class="text-danger">No Home Streams.</p>
                @endif
                @if (isset($game['gameinfo']['gameStreams']['ipad']['away']['vod-whole']['bitrate0']))
                    <p>
                    <h4>Away Streams</h4>
                    <a href="{{ str_replace('ipad', '4500', $game['gameinfo']['gameStreams']['ipad']['away']['vod-whole']['bitrate0']) }}" class="btn btn-primary">4500</a>
                    <a href="{{ str_replace('ipad', '3000', $game['gameinfo']['gameStreams']['ipad']['away']['vod-whole']['bitrate0']) }}" class="btn btn-primary">3000</a>
                    <a href="{{ str_replace('ipad', '1600', $game['gameinfo']['gameStreams']['ipad']['away']['vod-whole']['bitrate0']) }}" class="btn btn-primary">1600</a>
                    <a href="{{ str_replace('ipad', '800', $game['gameinfo']['gameStreams']['ipad']['away']['vod-whole']['bitrate0']) }}" class="btn btn-primary">800</a>
                    <h4>Away Downloads</h4>
                    <a href="{{ str_replace('.cdnak.','.cdnl3nl.', str_replace('.m3u8','', str_replace('ipad', '5000', $game['gameinfo']['gameStreams']['ipad']['away']['vod-whole']['bitrate0']))) }}" class="btn btn-warning">5000</a>
                    <a href="{{ str_replace('.cdnak.','.cdnl3nl.', str_replace('.m3u8','', str_replace('ipad', '3000', $game['gameinfo']['gameStreams']['ipad']['away']['vod-whole']['bitrate0']))) }}" class="btn btn-warning">3000</a>
                    <a href="{{ str_replace('.cdnak.','.cdnl3nl.', str_replace('.m3u8','', str_replace('ipad', '1600', $game['gameinfo']['gameStreams']['ipad']['away']['vod-whole']['bitrate0']))) }}" class="btn btn-warning">1600</a>
                    <a href="{{ str_replace('.cdnak.','.cdnl3nl.', str_replace('.m3u8','', str_replace('ipad', '800', $game['gameinfo']['gameStreams']['ipad']['away']['vod-whole']['bitrate0']))) }}" class="btn btn-warning">800</a>
                    </p>
                @else
                    <p class="text-danger">No Away Streams.</p>
                @endif

            @else
                @if ($game['gameinfo']['gameState'] == 1 || $game['gameinfo']['period'] == 0)
                    <p><button type="button" class="btn btn-info">Not started</button></p>
                @else
                    <p><button type="button" class="btn btn-success">In progress</button> <button type="button" class="btn btn-primary">{{ $game['gameinfo']['progressTime'] }}</button> <button type="button" class="btn btn-primary">{{ $game['gameinfo']['period'] }}</button></p>
                @endif
            @endif
        </div>
    @endforeach
@stop
